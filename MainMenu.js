
Emigranto.MainMenu = function (game) {

   this.music = null;
   this.playButton = null;

};

Emigranto.MainMenu.prototype = {

   create: function () {
      var ground = this.game.add.tileSprite(-50000, -50000, 900000, 900000, 'tile');
      this.bg = this.game.add.sprite(this.camera.width/2, this.camera.height/2, 'menu_bg');
      this.bg.fixedToCamera = true;
      this.bg.anchor.set(0.5);
      this.bg.scale.set(1.25);
      var backButton = this.game.add.sprite(this.camera.width/2, this.camera.height/2, 'menu_button');
      this.backButton = backButton;
      backButton.fixedToCamera = true;
      backButton.scale.set(2);
      backButton.anchor.set(0.5);
      backButton.inputEnabled = true;
      //this.group_hud.add(backButton);
      /*backButton.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2;
      };*/
      backButton.events.onInputOver.add(function() {
         backButton.frame = 1;
         playText.tint = 0x88FF88;
      }, this);
      backButton.events.onInputOut.add(function() {
         backButton.frame = 0;
         playText.tint = 0xFFFFFF;
      }, this);
      backButton.events.onInputDown.add(function() {
         backButton.frame = 1;
         playText.tint = 0x00FF00;
      }, this);
      backButton.events.onInputUp.add(function() {
         backButton.frame = 0;
         this.state.start('Game');
      }, this);

      var playText = this.game.add.bitmapText(this.camera.width/2-40, this.camera.height/2-30, 'impactBig', "PLAY", 60);
      this.playText = playText;
      //playText.anchor.set(0.5);
      //this.group_hud.add(playText);
      //playText.cameraOffset.y = 650;
      playText.align = 'center';
      playText.fixedToCamera = true;
      playText.cameraOffset.x = this.camera.width/2 - this.playText.textWidth/2;
      /*playText.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2 - _this.textWidth/2;
      }*/
   },

   update: function () {
      this.bg.cameraOffset.x = this.camera.width/2;
      this.backButton.cameraOffset.x = this.camera.width/2;
      this.backButton.cameraOffset.y = this.camera.height/2;
      this.playText.cameraOffset.x = this.camera.width/2 - this.playText.textWidth/2;
      this.playText.cameraOffset.y = this.camera.height/2-30;
   },

   startGame: function (pointer) {
      
   }

};
