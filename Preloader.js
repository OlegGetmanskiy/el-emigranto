

Emigranto.Preloader = function (game) {

   this.background = null;
   this.preloadBar = null;

   this.ready = false;

};

Emigranto.Preloader.prototype = {

   preload: function () {
      this.group_preloader = this.add.group();
      this.group_preloader.x = this.game.width/2;
      this.group_preloader.y = this.game.height/2;
      this.preloadBar = this.add.sprite(-1, 0, 'preloaderBar');
      var back1 = this.add.sprite(0,-24,'preloaderBack1');
      back1.anchor.set(0.5);
      var back2 = this.add.sprite(0,2,'preloaderBack2');
      back2.anchor.set(0.5);
      
      this.group_preloader.add(back1);
      this.group_preloader.add(back2);
      this.group_preloader.add(this.preloadBar);
      this.preloadBar.anchor.set(0.5);
      this.group_preloader.scale.set(1.15);
      this.load.setPreloadSprite(this.preloadBar);

      //this.load.bitmapFont('impact', prx + 'impact_0.png', prx + 'impact.fnt');
      this.load.bitmapFont('impact', prx + 'gill_0.png', prx + 'gill.fnt');
      //this.load.bitmapFont('impactBig', prx + 'impactBig_0.png', prx + 'impactBig.fnt');
      this.load.bitmapFont('impactBig', prx + 'gill_0.png', prx + 'gill.fnt');
      this.load.image('logo', prx + 'logo.png');
      this.load.image('black', prx + 'black.png');

      this.load.image('hero', prx + 'Hero.png');
      this.load.image('policeCar', prx + 'PoliceCar_1.png');
      this.load.image('tile', prx + 'Tile.png');
      this.load.spritesheet('obs1', prx + 'Obstacle_1.png', 89, 68, 10);
      this.load.image('obs2', prx + 'Obstacle_2.png');
      this.load.image('obs3', prx + 'ObstacleStone_1.png');
      this.load.image('obs4', prx + 'ObstacleStone_2.png');
      this.load.image('obs5', prx + 'ObstacleStone_3.png');

      this.load.image('bonus1', prx + 'Bonus_1.png');
      this.load.image('bonus2', prx + 'Bonus_2.png');
      this.load.image('bonus3', prx + 'Bonus_3.png');
      this.load.image('bonus4', prx + 'Bonus_4.png');
      this.load.image('bonus5', prx + 'Bonus_5.png');
      this.load.image('bonusBg', prx + 'BonusBg.png');
      this.load.spritesheet('shield', prx + 'shield.png', 63, 95);
      
      this.load.image('mine', prx + 'mine.png');

      this.load.image('bs', prx + 'jB.png');
      this.load.image('js', prx + 'jC.png');

      this.load.image('score_panel', prx + 'score_panel.png');
      this.load.spritesheet('menu_button', prx + 'menu_button.png', 160, 56);
      this.load.image('menu_bg', prx + 'menu_bg.png');

      this.load.image('grass1', prx + 'grass_1.png');

      this.load.spritesheet('explosion', prx + 'explosion.png', 96, 96);
      this.load.spritesheet('explosion_police', prx + 'explosion_police.png', 173, 192);
   },

   create: function () {
      // Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
      this.preloadBar.cropEnabled = false;

   },

   update: function () {
      this.group_preloader.x = this.game.width/2;
      this.group_preloader.y = this.game.height/2;
      // You don't actually need to do this, but I find it gives a much smoother game experience.
      // Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
      // You can jump right into the menu if you want and still play the music, but you'll have a few
      // seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
      // it's best to wait for it to decode here first, then carry on.
      
      // If you don't have any music in your game then put the game.state.start line into the create function and delete
      // the update function completely.
      
      //if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
      //{
         this.ready = true;
         this.state.start('MainMenu');
      //}
      
   }

};
