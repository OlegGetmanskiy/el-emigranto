var random = new Phaser.RandomDataGenerator([new Date()]);

Emigranto.doResize = function() {

}

Emigranto.Game = function (game) {

    //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

   this.game;      //  a reference to the currently running game
   this.add;       //  used to add sprites, text, groups, etc
   this.camera;    //  a reference to the game camera
   this.cache;     //  the game cache
   this.input;     //  the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
   this.load;      //  for preloading assets
   this.math;      //  lots of useful common math operations
   this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc
   this.stage;     //  the game stage
   this.time;      //  the clock
   this.tweens;    //  the tween manager
   this.state;     //  the state manager
   this.world;     //  the game world
   this.particles; //  the particle manager
   this.physics;   //  the physics manager
   this.rnd;       //  the repeatable random number generator

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.
};

Emigranto.Game.prototype = {

   create: function () {

      this.noEnemies = false;

      this.maxEnemies = 1;
      this.lost = false;
      this.finalScore = 0;

      this.playerExp = 0;
      this.playerExpFactor = 1;

      this.group_ground = this.game.add.group();
      this.group_items = this.game.add.group();
      this.group_enemies = this.game.add.group();
      this.group_player = this.game.add.group();
      this.group_hud = this.game.add.group();

      this.xSpawnRange = this.game.width/2 + 100;
      this.xSpawnRange2 =this.game.width/2 + 150;
      this.ySpawnRange = this.game.height/2 + 100; 

      this.xCheckRange = this.xSpawnRange + 100;
      this.yCheckRange = this.ySpawnRange + 100;

      this.expOrder = 1;

      if(!mobile) {
         /*this.xSpawnRange = 700;
         this.xSpawnRange2 =750;
         this.ySpawnRange = 500; 

         this.xCheckRange = 800;
         this.yCheckRange = 600;*/

         this.maxItems = 20;

         //Джойстик не нужен на десктопе
         this.joystick = new Joystick(-100, -100, 75, this.game, this.group_hud);
         this.policeBaseSpeed = 340;   //default 340
         this.playerBaseSpeed = 320;   //default 320

         this.playerRotationFactor = 3.5;
         this.policeRotationFactor = 1.25;
         this.playerManeuverFactor = 0;
         this.policeManeuverFactor = 10;
      } else {
         /*this.xSpawnRange = 300;
         this.xSpawnRange2 = 400;
         this.ySpawnRange = 500; 
         this.xCheckRange = 500;
         this.yCheckRange = 550;*/

         this.maxItems = 20;

         this.joystick = new Joystick(game.width-95, game.height-95, 75, this.game, this.group_hud);
         this.joystick.sprite.doResize = function(_this) {
            this.joystick.x = this.camera.width - 95;
            this.joystick.y = this.camera.height - 95;
            _this.cameraOffset.x = this.camera.width - 95;
            _this.cameraOffset.y = this.camera.height - 95;
         }
         this.joystick.backgraund.doResize = this.joystick.sprite.doResize;

         this.policeBaseSpeed = 295;   //default 320
         this.playerBaseSpeed = 275;   //default 300

         this.playerRotationFactor = 4.5;
         this.policeRotationFactor = 1.1;
         this.playerManeuverFactor = 0;
         this.policeManeuverFactor = 10;
      }

      this.enemies = [];
      this.items = [];
      this.bonuses = [];
      this.enemiesCount = 0;
      this.itemsCount = 0;
      this.godmode = false;
      
      var ground = this.game.add.tileSprite(0, 0, 900000, 900000, 'tile');
      ground.scale.set(1);
      this.group_ground.add(ground);
      this.game.world.setBounds(0, 0, 900000, 900000);
      this.game.physics.startSystem(Phaser.Physics.P2JS);
      this.game.physics.startSystem(Phaser.Physics.ARCADE);

      var black = this.game.add.tileSprite(0, 0, 900000, 900000, 'black');
      this.black = black;
      black.alpha = 0.5;
      this.group_hud.add(black);
      /*this.game.add.tween(black).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.Out, true);
      this.time.events.add(Phaser.Timer.SECOND * 1, function() {
         black.visible = false;
      }, this);*/
      black.visible = false;

      this.playerCollisionGroup = this.game.physics.p2.createCollisionGroup();
      this.enemyCollisionGroup = this.game.physics.p2.createCollisionGroup();
      this.itemsCollisionGroup = this.game.physics.p2.createCollisionGroup();
      this.game.physics.p2.setImpactEvents(true);

      this.player = this.createPlayer();

      this.game.camera.follow(this.player);

      this.time.events.loop(Phaser.Timer.SECOND * 0.01, this.updatePlayer, this);
      this.time.events.loop(Phaser.Timer.SECOND * 0.01, function() {
         this.updateEnemies();
      }, this);
      this.time.events.loop(Phaser.Timer.SECOND * 0.5, function() {
         this.regulateEnemies();
      }, this);
      this.time.events.loop(Phaser.Timer.SECOND * 0.1, this.regulateItems, this);
      this.time.events.loop(Phaser.Timer.SECOND * 3, this.spawnBonus, this);

      this.time.events.add(Phaser.Timer.SECOND * 15, function() {
         this.maxEnemies = 2;
      }, this);
      this.time.events.add(Phaser.Timer.SECOND * 45, function() {
         this.maxEnemies = 3;
      }, this);
      this.time.events.add(Phaser.Timer.SECOND * 85, function() {
         this.maxEnemies = 4;
      }, this);
      this.time.events.add(Phaser.Timer.SECOND * 125, function() {
         this.maxEnemies = 5;
      }, this);

      if(!mobile) {
         this.time.events.add(Phaser.Timer.SECOND * 155, function() {
            this.maxEnemies = 6;
         }, this);
         this.time.events.add(Phaser.Timer.SECOND * 195, function() {
            this.maxEnemies = 7;
         }, this);
         this.time.events.add(Phaser.Timer.SECOND * 255, function() {
            this.maxEnemies = 8;
         }, this);
         this.time.events.add(Phaser.Timer.SECOND * 340, function() {
            this.maxEnemies = 9;
         }, this);
      }

      this.time.events.loop(Phaser.Timer.SECOND * 0.1, function() {
         this.resizeGame();
      }, this);

      var basicPlayerRotationFactor = this.playerRotationFactor;

      this.doubleExpTime = 0;
      this.rotationBonusTime = 0;
      this.godmodeTime = 0;
      
      this.time.events.loop(Phaser.Timer.SECOND * 0.05, function() {
         if(this.lost) return;
         this.addExp(1);
      }, this);
      this.time.events.loop(Phaser.Timer.SECOND * 0.05, function() {
         if(this.lost) return;
         if(this.doubleExpTime > 0) {
            this.doubleExpTime -= 0.05;
            this.playerExpFactor = 2;
            this.scoreText.tint = 0xFF5300;
         } else {
            this.playerExpFactor = 1;
            this.doubleExpTime = 0;
            this.scoreText.tint = 0xFFFFFF;
         }

         if(this.rotationBonusTime > 0) {
            this.rotationBonusTime -= 0.05;
            this.playerRotationFactor = basicPlayerRotationFactor * 1.7;
         } else {
            this.playerRotationFactor = basicPlayerRotationFactor;
            this.rotationBonusTime = 0;
         }

         if(this.godmodeTime > 0) {
            this.playerShield.visible = true;
            this.godmodeTime -= 0.05;
            this.godmode = true;
            if(this.godmodeTime <= 2)
               this.playerShield.animations.play('flick');
         } else {
            this.playerShield.visible = false;
            this.godmode = false;
            this.godmodeTime = 0;
         }
      }, this);

      this.scorePanel = this.game.add.sprite(this.camera.width/2, 80, 'score_panel');
      this.scorePanel.scale.set(1.25);
      this.scorePanel.anchor.setTo(0.5, 1);
      this.scorePanel.fixedToCamera = true;
      this.scoreText = this.game.add.bitmapText(this.camera.width/2, 0, 'impact', "0", 36);
      this.scoreText.fixedToCamera = true;
      //this.scoreText.anchor.set(0.5);
      this.group_hud.add(this.scorePanel);
      this.group_hud.add(this.scoreText);
      this.scoreText.cameraOffset.y = 5;
      this.scoreText.align = 'center';
      this.scoreText.cameraOffset.x = this.camera.width/2 - this.scoreText.textWidth/2;
      this.scoreText.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2 - _this.textWidth/2;
      }
      this.scorePanel.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2;
      }

      this.bestScoreText = this.game.add.bitmapText(this.camera.width/2, 200, 'impact', this.loadBestScore(), 28);
      this.bestScoreText.fixedToCamera = true;
      //this.bestScoreText.anchor.set(0.5);
      this.group_hud.add(this.bestScoreText);
      this.bestScoreText.align = 'center';
      this.bestScoreText.tint = 0xffcc00;
      this.bestScoreText.cameraOffset.y = 45;
      this.bestScoreText.cameraOffset.x = this.camera.width/2 - this.bestScoreText.textWidth/2;
      this.bestScoreText.doResize = this.scoreText.doResize;
   },

   loose : function() {
      if(this.lost) return;

      this.lost = true;
      this.player.body.static = true;
      this.policeBaseSpeed = 0;
      this.policeRotationFactor = 0;
      this.policeManeuverFactor = 0;
      this.enemies.forEach(function(enemy) {
         this.game.add.tween(enemy).to({ baseSpeed: 0 }, 500, Phaser.Easing.Linear.Out, true);
      }, this);
      this.finalScore = this.playerExp;
      console.log('final', this.finalScore);
      //this.black.visible = true;
      //this.game.add.tween(this.black).to({ alpha: 0.5 }, 400, Phaser.Easing.Linear.Out, true);
      
      var backButton = this.game.add.sprite(this.camera.width/2, this.camera.height * 0.7, 'menu_button');
      backButton.scale.set(2);
      backButton.fixedToCamera = true;
      backButton.cameraOffset.x = this.camera.width/2;
      backButton.anchor.set(0.5);
      //backButton.width = 250;
      //backButton.height = 80;
      backButton.inputEnabled = true;
      this.group_hud.add(backButton);
      backButton.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2;
         _this.cameraOffset.y = this.camera.height * 0.7;
      };
      backButton.events.onInputOver.add(function() {
         backButton.frame = 1;
         tryAgain.tint = 0xFFFF88;
      }, this);
      backButton.events.onInputOut.add(function() {
         backButton.frame = 0;
         tryAgain.tint = 0xFFFFFF;
      }, this);
      backButton.events.onInputDown.add(function() {
         backButton.frame = 1;
         tryAgain.tint = 0x00FF00;
      }, this);
      backButton.events.onInputUp.add(function() {
         backButton.frame = 0;
         this.state.start('Game');
      }, this);

      var tryAgain = this.game.add.bitmapText(this.camera.width/2, this.camera.height * 0.7-30, 'impactBig', "TRY AGAIN", 60);
      tryAgain.fixedToCamera = true;
      //tryAgain.anchor.set(0.5);
      this.group_hud.add(tryAgain);
      tryAgain.cameraOffset.y = 450;
      tryAgain.align = 'center';
      tryAgain.cameraOffset.x = this.camera.width/2 - this.scoreText.textWidth/2;
      tryAgain.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2 - _this.textWidth/2;
         _this.cameraOffset.y = this.camera.height * 0.7-30;
      }
      tryAgain.doResize.apply(this, [tryAgain]);

      this.saveBestScore();
      resizeGame();
   },

   createPlayer : function() {
      var sprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'hero');
      sprite.anchor.setTo(0.5, 0.5);
      sprite.scale.setTo(1.2, 1.2);

      var shield = this.game.add.sprite(this.camera.width/2, this.camera.height/2, 'shield');
      shield.visible = false;
      shield.scale.set(1);
      shield.anchor.setTo(0.45, 0.48);
      shield.doResize = function(_this) {
         _this.cameraOffset.x = this.camera.width/2;
         _this.cameraOffset.y = this.camera.height/2;
      };
      shield.fixedToCamera = true;
      shield.animations.add('flick', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 30);
      this.playerShield = shield;
      //sprite.alpha = 0.5;
      this.game.physics.p2.enable(sprite, false);
      //this.game.physics.arcade.enable(sprite);

      sprite.body.kinematic = false;

      sprite.body.setRectangle(53, 8, 6, -2);
      sprite.body.setCollisionGroup(this.playerCollisionGroup);
      sprite.body.collides(this.enemyCollisionGroup, function(hero, police) {
         if(!this.godmode) {
            this.loose();
            police.static = true;
         } else {
            if(police.sprite) {
               var i = this.enemies.indexOf(police.sprite);
               this.enemies.splice(this.enemies.indexOf(police.sprite), 1);
               this.playPoliceExplosion(police.sprite.x, police.sprite.y);
               this.animateText(police.sprite.x, police.sprite.y, 'impactBig', 60, 0x44FF44, 25);
               police.sprite.destroy();
               this.enemiesCount -= 1;
               this.addExp(25);
            }

            this.player.body.static = true;
            this.player.body.static = false;
            this.player.body.setZeroDamping();
            this.player.body.setZeroForce();
            //this.player.setZeroVelocity();
            this.player.body.setZeroRotation();
            this.updatePlayer();
         }
      }, this);
      sprite.body.collides(this.itemsCollisionGroup, function(player, item) {
         player.setZeroDamping();
         player.setZeroForce();
         //player.setZeroVelocity();
         player.setZeroRotation();
         this.updatePlayer();
      }, this);

      sprite.baseSpeed = this.playerBaseSpeed;
      this.group_player.add(sprite);
      this.group_hud.add(shield);
      return sprite;
   },

   createEnemy : function(x, y, rotation) {
      var sprite = this.game.add.sprite(x, y, 'policeCar');
      sprite.anchor.setTo(0.5, 0.5);
      sprite.scale.set(0.8);
      this.game.physics.p2.enable(sprite, false);

      sprite.body.kinematic = false;
      sprite.body.damping = 0;
      sprite.body.setZeroDamping();
      sprite.body.setZeroForce();
      sprite.body.setZeroVelocity();

      //sprite.body.collideWorldBounds = true;
      sprite.body.setRectangle(67, 34, 6, -2);

      sprite.body.setCollisionGroup(this.enemyCollisionGroup);
      sprite.body.collides(this.enemyCollisionGroup, function(killer, victim) {

         var i = this.enemies.indexOf(killer);
         this.enemies.splice(this.enemies.indexOf(killer.sprite), 1);
         this.playPoliceExplosion(killer.sprite.x, killer.sprite.y);
         this.animateText(killer.sprite.x, killer.sprite.y, 'impactBig', 60, 0x44FF44, 25);
         killer.sprite.destroy();
         this.enemiesCount -= 1;
         this.addExp(25);
      }, this);
      sprite.body.collides(this.playerCollisionGroup, null, this);
      sprite.body.collides(this.itemsCollisionGroup, function(car, item) {
         car.setZeroDamping();
         car.setZeroForce();
         //car.setZeroVelocity();
         car.setZeroRotation();
      }, this);

      sprite.body.rotation = this.game.physics.arcade.angleBetween(sprite, this.player);

      this.group_enemies.add(sprite);
      sprite.baseSpeed = this.policeBaseSpeed;
      return sprite;
   },

   updatePlayer : function() {
      if(this.player.body.static) return;
      if(this.lost) {
         this.player.body.velocity.x = 0;
         this.player.body.velocity.y = 0;
         return;
      }
      //_this.rotation = homingMissle.lerp_dir(_this.rotation, targetAngle, 0.30);
      //targetAngle = this.game.physics.arcade.angleToPointer(this.player);
      if(this.distanceBetween(this.game.input.activePointer,this.joystick) > 75) {
         targetAngle = this.game.physics.arcade.angleToPointer(this.player);
      } else
         targetAngle = this.joystick.angle / 180 * Math.PI;

      var delta = Math.abs(targetAngle - this.player.body.rotation);
      var speed = this.player.baseSpeed - delta * this.playerManeuverFactor;

      this.player.body.rotation = this.lerpDir(this.player.body.rotation * 180 / Math.PI, targetAngle * 180 / Math.PI, this.playerRotationFactor);
      this.playerShield.angle = this.player.body.rotation*180/Math.PI+90;
      //_this.body.velocity.x = Math.cos(_this.angle / 180 * Math.PI) * this.speed;
      //_this.body.velocity.y = Math.sin(_this.angle / 180 * Math.PI) * this.speed;
      this.player.body.velocity.x = Math.cos(this.player.body.rotation) * speed;
      this.player.body.velocity.y = Math.sin(this.player.body.rotation) * speed;
   },

   updateEnemies : function() {
      
      function updateOne(enemy) {
         if(enemy.body.static) return;

         if(Math.abs(enemy.x - this.player.x) > this.xCheckRange || Math.abs(enemy.y - this.player.y) > this.yCheckRange) {
            var i = this.enemies.indexOf(enemy);
            this.enemies.splice(this.enemies.indexOf(enemy), 1);
            enemy.destroy();
            this.enemiesCount -= 1;
         }

         var targetAngle = this.game.physics.arcade.angleBetween(enemy, this.player);
         var delta = Math.abs(targetAngle - enemy.body.rotation);
         var speed = enemy.baseSpeed - delta * this.policeManeuverFactor;

         enemy.body.rotation = this.lerpDir(enemy.body.rotation * 180 / Math.PI, targetAngle * 180 / Math.PI, this.policeRotationFactor);
         //_this.body.velocity.x = Math.cos(_this.angle / 180 * Math.PI) * this.speed;
         //_this.body.velocity.y = Math.sin(_this.angle / 180 * Math.PI) * this.speed;
         enemy.body.velocity.x = Math.cos(enemy.body.rotation) * speed;
         enemy.body.velocity.y = Math.sin(enemy.body.rotation) * speed;
      }

      this.enemies.forEach(function(enemy) {
         updateOne.apply(this, [enemy]);
      }, this);
   },

   createItem : function(item, x, y, nocheck) {
      var sprite = this.game.add.sprite(x, y, item.name);
      sprite.anchor.setTo(0.5, 0.5);
      this.game.physics.p2.enable(sprite, false);
      if(item.name == 'obs1') {
         sprite.scale.setTo(1.5, 1.5);
         sprite.animations.add('crush', [1,2,3,4,5,6,7,8,9], 30);
         sprite.body.setRectangle(70, 5, 0 ,15);
         sprite.body.addRectangle(8, 24, -20, 15);
         sprite.body.addRectangle(8, 24, 20, 15);
      }
      if(item.name == 'obs2') {
         sprite.scale.setTo(1.5, 1.5);
         sprite.body.setCircle(22, -1, -3);
      }
      if(item.name == 'obs3') {
         sprite.body.setCircle(18, -7, -7);
         sprite.body.addCircle(18, 3, 3);
      }
      if(item.name == 'obs4') {
         sprite.body.setCircle(26, -4, 0);
      }
      if(item.name == 'obs5') {
         sprite.body.setCircle(32, -3, -3);
         sprite.body.addCircle(25, -12, 13);
         sprite.body.addCircle(15, 25, 0);
      }
      if(item.name == 'bonus2') {
         sprite.body.setCircle(20, -4, -4);
      }

      sprite.body.kinematic = false;
      sprite.body.static = true;
      sprite.body.rotation = item.rotation;
      //sprite.body.rotation = 0

      sprite.body.setCollisionGroup(this.itemsCollisionGroup);

      sprite.body.collides(this.playerCollisionGroup, function(itemBody, player) {
         if(item.name == 'obs2') {
            this.game.physics.p2.removeBody(itemBody);
            player.sprite.baseSpeed = 100;
            this.game.add.tween(player.sprite).to({ baseSpeed: this.playerBaseSpeed }, 500, Phaser.Easing.Linear.Out, true);
         }
         if(item.name == 'obs1') {
            itemBody.sprite.animations.play('crush');
            itemBody.rotation = player.rotation + Math.PI/2;
            this.game.physics.p2.removeBody(itemBody);
            player.sprite.baseSpeed = 100;
            //var rotFactor = this.playerRotationFactor;
            this.game.add.tween(player.sprite).to({ baseSpeed: this.playerBaseSpeed }, 500, Phaser.Easing.Linear.Out, true);
            //this.game.add.tween(this).to({ playerRotationFactor: rotFactor }, 500, Phaser.Easing.Linear.Out, true);
         }
         if(item.name == 'obs3' || item.name == 'obs4' || item.name == 'obs5') {
            this.loose();
         }
         if(item.name == 'mine') {
            player.static = true;
            player.static = false;
         }
      }, this);

      if(item.name == 'obs1') {
         sprite.body.collides(this.enemyCollisionGroup, function(itemBody, car) {
            itemBody.sprite.animations.play('crush');
            itemBody.rotation = car.rotation + Math.PI/2;
            this.game.physics.p2.removeBody(itemBody);

            if(!car || !car.sprite) return;
            car.sprite.baseSpeed = 200;
            this.game.add.tween(car.sprite).to({ baseSpeed: this.policeBaseSpeed }, 500, Phaser.Easing.Linear.Out, true);
         }, this);
      }

      if(item.name == 'obs2') {
         sprite.body.collides(this.enemyCollisionGroup, function(itemBody, car) {
            this.game.physics.p2.removeBody(itemBody);
            
            if(!car || !car.sprite) return;
            car.sprite.baseSpeed = 200;
            this.game.add.tween(car.sprite).to({ baseSpeed: this.policeBaseSpeed }, 500, Phaser.Easing.Linear.Out, true);
         }, this);
      }

      if(item.name == 'obs3' || item.name == 'obs4' || item.name == 'obs5') {
         sprite.body.collides(this.enemyCollisionGroup, function(itemBody, car) {
            if(!car || !car.sprite) return;
            var i = this.enemies.indexOf(car.sprite);
            if(i == -1) return;
            this.enemies.splice(i, 1);
            this.playPoliceExplosion(car.sprite.x, car.sprite.y);
            this.animateText(car.sprite.x, car.sprite.y, 'impactBig', 60, 0x44FF44, 25);
            car.sprite.destroy();
            this.enemiesCount -= 1;
            this.addExp(25);
         }, this);
      }

      if(item.name == 'mine') {
         sprite.body.collides(this.enemyCollisionGroup, function(itemBody, car) {
            this.game.physics.p2.removeBody(itemBody);
            itemBody.sprite.visible = false;
            if(!car || !car.sprite) return;
            var i = this.enemies.indexOf(car.sprite);
            if(i == -1) return;
            this.enemies.splice(i, 1);
            this.playPoliceExplosion(car.sprite.x, car.sprite.y);
            this.animateText(car.sprite.x, car.sprite.y, 'impactBig', 60, 0x44FF44, 25);
            car.sprite.destroy();
            this.enemiesCount -= 1;
            this.addExp(25);
         }, this);
      }

      this.group_items.add(sprite);
      return sprite;
   },

   createBonus : function(item, x, y, nocheck) {
      var sprite = this.game.add.sprite(x, y, item.name);
      sprite.scale.setTo(1.3, 1.3);
      sprite.anchor.setTo(0.5, 0.5);
      this.game.physics.p2.enable(sprite, false);

      sprite.childBase = this.game.add.sprite(x, y, 'bonusBg');
      sprite.childBase.anchor.setTo(0.5, 0.5);
      sprite.childBase.scale.setTo(1.3, 1.3);
      
      this.game.physics.arcade.enable(sprite.childBase, false);
      sprite.childBase.body.angularVelocity = 100;
      this.group_ground.add(sprite.childBase);

      sprite.body.setCircle(22, 0, 0);

      sprite.body.kinematic = false;
      sprite.body.static = true;
      sprite.body.rotation = 0

      sprite.body.setCollisionGroup(this.itemsCollisionGroup);

      sprite.body.collides(this.playerCollisionGroup, function(itemBody, player) {
         this.game.physics.p2.removeBody(itemBody);
         itemBody.sprite.visible = false;
         itemBody.sprite.childBase.visible = false;
         item.action.apply(this);

         var anim = this.game.add.sprite(this.game.width/2, this.game.height/2, item.name);
         anim.fixedToCamera = true;
         anim.anchor.set(0.5);
         anim.scale.set(1.4);
         this.group_hud.add(anim);
         this.game.physics.arcade.enable(anim);
         this.game.add.tween(anim.scale).to({ x: 3, y: 3 }, 250, Phaser.Easing.Linear.Out, true);
         this.time.events.add(Phaser.Timer.SECOND * 0.250, function() {
            this.game.add.tween(anim).to({ alpha: 0 }, 250, Phaser.Easing.Linear.Out, true);
            this.game.add.tween(anim.scale).to({ x: 0, y: 0 }, 250, Phaser.Easing.Linear.Out, true);
         }, this);
         this.time.events.add(Phaser.Timer.SECOND * 0.501, function() {
            anim.destroy();
         }, this);
      }, this);

      this.group_items.add(sprite);
      return sprite;
   },

   godmodeTime : 0,

   spawnBonus : function() {
      var randName = ['bonus1', 'bonus2', 'bonus3', 'bonus4', 'bonus5'][random.integerInRange(0,4)];
      //var randName = 'bonus1';
      var actionFunc;
      if(randName == 'bonus1')
         actionFunc = function() {
            this.godmodeTime += 7;
         }
      if(randName == 'bonus2')
         actionFunc = function() {
            this.leaveMines();
         }
      if(randName == 'bonus3')
         actionFunc = function() {
            this.killAll();
         }
      if(randName == 'bonus4')
         actionFunc = function() {
            this.doubleExp();
         }
      if(randName == 'bonus5')
         actionFunc = function() {
            this.rotationBonus();
         }

      var randX;
      var randY;
      if(random.integerInRange(0,1) == 1) {
         randX = this.player.x + random.integerInRange(-this.xSpawnRange, this.xSpawnRange), 
         randY = this.player.y - [-this.ySpawnRange, this.ySpawnRange][random.integerInRange(0,1)]
      } else {
         randX = this.player.x - [-this.xSpawnRange2, this.xSpawnRange2][random.integerInRange(0,1)], 
         randY = this.player.y + random.integerInRange(-this.ySpawnRange, this.ySpawnRange)
      }

      var pass = true;
      var _this = {x: randX, y: randY};
      this.items.forEach(function(item) {
         if(this.distanceBetween(_this, item) < 100) {
            pass = false;
         }
      }, this);
      this.bonuses.forEach(function(item) {
         if(this.distanceBetween(_this, item) < 100) {
            pass = false;
         }
      }, this);

      if(pass) {
         this.bonuses.push(
            this.createBonus(
               { name: randName, rotation: 0, action: actionFunc },
               randX,
               randY
         ));

         this.bonuses.forEach(function(bonus) {
            if(Math.abs(bonus.x - this.player.x) > this.xCheckRange || Math.abs(bonus.y - this.player.y) > this.yCheckRange) {
               var i = this.bonuses.indexOf(bonus);
               this.bonuses.splice(i, 1);
               bonus.childBase.destroy();
               bonus.destroy();
            }
         }, this);
      }
   },

   regulateItems : function() {
      //return;
      if(this.itemsCount < this.maxItems) {
         var randX;
         var randY;

         if(random.integerInRange(0,1) == 1) {
            randX = this.player.x + random.integerInRange(-this.xSpawnRange, this.xSpawnRange), 
            randY = this.player.y - [-this.ySpawnRange, this.ySpawnRange][random.integerInRange(0,1)]
         } else {
            randX = this.player.x - [-this.xSpawnRange2, this.xSpawnRange2][random.integerInRange(0,1)], 
            randY = this.player.y + random.integerInRange(-this.ySpawnRange, this.ySpawnRange)
         }

         var pass = true;
         var _this = {x: randX, y: randY};
         this.items.forEach(function(item) {
            if(this.distanceBetween(_this, item) < 100) {
               pass = false;
            }
         }, this);

         if(pass) {
            var randName;
            if(random.integerInRange(0,5) == 0)
               randName = ['obs3', 'obs4', 'obs5'][random.integerInRange(0,2)];
            else
               randName = ['obs1', 'obs2'][random.integerInRange(0,1)];

            this.items.push(
               this.createItem(
                  { name: randName, rotation: random.integerInRange(0, 180) },
                  randX, 
                  randY
            ));
            this.itemsCount++;
         }
      }

      this.items.forEach(function(item) {
         if(!item) return;
         if(Math.abs(item.x - this.player.x) > this.xCheckRange || Math.abs(item.y - this.player.y) > this.yCheckRange) {
            var i = this.items.indexOf(item);
            this.items.splice(this.items.indexOf(item), 1);
            item.destroy();
            this.itemsCount -= 1;
         }
      }, this);
   },

   regulateEnemies : function() {
      //return;
      if(this.noEnemies) return;
      if(this.enemiesCount < this.maxEnemies) {
         var rnd = [0,1][random.integerInRange(0,1)];
         if(rnd == 1)
            this.enemies.push(
               this.createEnemy(
                  this.player.x + random.integerInRange(-this.xSpawnRange, this.xSpawnRange), 
                  this.player.y - [-this.ySpawnRange, this.ySpawnRange][random.integerInRange(0,1)]
            ));
         else
            this.enemies.push(
               this.createEnemy(
                  this.player.x + [-this.xSpawnRange, this.xSpawnRange][random.integerInRange(0,1)], 
                  this.player.y - random.integerInRange(-this.ySpawnRange, this.ySpawnRange)
            ));
         this.enemiesCount++;
      }
   },

   leaveMines : function() {
      this.game.time.events.repeat(Phaser.Timer.SECOND * 0.75, 5, function() {
         this.items.push(
               this.createItem(
                  { name: 'mine', rotation: 0 },
                  this.player.x, 
                  this.player.y,
                  true
            ));
      }, this);
   },

   killAll : function() {
      this.enemies.forEach(function(enemy, index) {
         this.playPoliceExplosion(enemy.x, enemy.y);
         this.animateText(enemy.x, enemy.y, 'impactBig', 60, 0x44FF44, 25);
         this.addExp(25);
         enemy.destroy();
      }, this);
      this.enemies.splice(0, this.enemies.length);
      this.enemiesCount = 0;
   },

   doubleExpTime : 0,
   doubleExp : function() {
      this.playerExpFactor = 2;
      this.doubleExpTime += 10;
   },

   rotationBonusTime : 0,
   rotationBonus : function() {
      this.rotationBonusTime += 5;
   },

   expOrder : 1,
   addExp : function(amount) {
      this.playerExp += amount * this.playerExpFactor;
      this.scoreText.text = this.playerExp.toString();
      this.scoreText.doResize.apply(this, [this.scoreText]);
   },

   playExplosion : function(x, y) {
      var anim = this.game.add.sprite(x, y, 'explosion');
      anim.anchor.set(0.5);
      anim.scale.set(2.5);
      this.group_player.add(anim);
      anim.animations.add('expl', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 30);
      anim.rotation = random.integerInRange(0, 180);
      anim.animations.play('expl')
      this.time.events.add(Phaser.Timer.SECOND * 0.5, function() {
         anim.animations.stop('expl');
         anim.visible = false;
         anim.destroy();
      }, this);
   },

   playPoliceExplosion : function(x, y) {
      var anim = this.game.add.sprite(x, y, 'explosion_police');
      anim.anchor.set(0.5);
      anim.scale.set(1);
      this.group_player.add(anim);
      anim.animations.add('expl', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 60);
      anim.rotation = random.integerInRange(0, 180);
      anim.animations.play('expl')
      this.time.events.add(Phaser.Timer.SECOND * 0.5, function() {
         anim.animations.stop('expl');
         anim.visible = false;
         anim.destroy();
      }, this);
   },

   animateText : function(x, y, font, size, color, text) {
      if(this.lost) return;
      var _text = this.game.add.bitmapText(x-20, y, font, text.toString(), size);
      _text.tint = color;
      this.group_hud.add(_text);
      this.game.add.tween(_text).to({ y: _text.y-50, alpha: 0 }, 1000, Phaser.Easing.Linear.Out, true);
      this.time.events.add(Phaser.Timer.SECOND * 1, function() {
         _text.destroy();
      }, this);
   },

   update : function () {
      this.joystick.update();
   },

   render : function() {
      
   },
   lastWidth : 0,
   resizeGame : function() {
      if(this.lastWidth == this.game.width) return;
      this.xSpawnRange = this.game.width/2 + 100;
      this.xSpawnRange2 = this.game.width/2 + 150;
      this.ySpawnRange = this.game.height/2 + 100; 

      this.xCheckRange = this.xSpawnRange + 100;
      this.yCheckRange = this.ySpawnRange + 100;

      this.group_hud.forEach(function(elem) {
         if(elem.doResize)
            elem.doResize.apply(this, [elem]);
      }, this);   
      this.lastWidth = this.game.width;
   },

   quitGame: function (pointer) {
      this.state.start('MainMenu');
   },

   loadBestScore: function() {
      var key = SAVE_PREFIX + 'best';
      return $.jStorage.get(key, "0").toString();
   },

   saveBestScore: function() {
      var key = SAVE_PREFIX + 'best';
      var newScore = this.playerExp;
      if(newScore > this.loadBestScore())
         $.jStorage.set(key, newScore.toString());
   },

   lerpDir : function(cur_dir, tar_dir, inc) {
      if(cur_dir > 360)
         cur_dir-=360;
      if(cur_dir < -360)
         cur_dir+=360;
      if (Math.abs( tar_dir - cur_dir) <= inc || Math.abs( tar_dir - cur_dir) >= (360 - inc)) {
         if(cur_dir <= 360 && cur_dir >= -360) {
            cur_dir = tar_dir;
         }
      } else {
         if (Math.abs( tar_dir - cur_dir) > 180) {
            if (tar_dir < cur_dir) {
               tar_dir += 360;
            } else {
               tar_dir -= 360;
            }
         }
         if ( tar_dir > cur_dir) {
            cur_dir += inc;
         } else {
            if ( tar_dir < cur_dir) {
               cur_dir -= inc;
            }
         }
      }
      return cur_dir / 180 * Math.PI;
   },

   distanceBetween : function(obj1, obj2) {
      if(!obj1 || !obj2) return 999;
      return Math.sqrt(((obj1.x - obj2.x) * (obj1.x - obj2.x)) + ((obj1.y - obj2.y)*(obj1.y - obj2.y)));
   }

};
