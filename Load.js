var winWidth = $(window).width(),
    winHeight = $(window).height();
var winNewWidth = winWidth,
    winNewHeight = winHeight;

$(window).resize(window.resizeGame);

window.onload = function() {
   var forceMobile = false;
   if(FULLSCREEN)
      var device = new Phaser.Device();
   else
      var device = { desktop: true };

   var height = $(window).height();
   var width = $(window).width();
   
   if(device.desktop && !forceMobile) {
      mobile = false;
      //var game = new Phaser.Game(1160, 870, Phaser.AUTO, 'el-emigranto');
      var gameWidth = width * 870 / height;
      if(gameWidth > 870*2.4)
         gameWidth = 870*2.4;
      if(gameWidth < 870*4/3)
         gameWidth = 870*4/3;
      
      if(FULLSCREEN)
         game = new Phaser.Game(gameWidth, 870, Phaser.CANVAS, 'wildpursuit');
      else
         game = new Phaser.Game(1160, 870, Phaser.CANVAS, 'wildpursuit');
   } else {
      mobile = true;
      if(height > width) {
         var gameHeight = height / width * 600;
         game = new Phaser.Game(600, gameHeight, Phaser.CANVAS, 'wildpursuit');
      } else {
         var gameWidth = width / height * 600;
         game = new Phaser.Game(gameWidth, 600, Phaser.CANVAS, 'wildpursuit');
      }
   }

   //  Add the States your game has.
   //  You don't have to do this in the html, it could be done in your Boot state too, but for simplicity I'll keep it here.
   game.state.add('Boot', Emigranto.Boot);
   game.state.add('Preloader', Emigranto.Preloader);
   game.state.add('Logo', Emigranto.Logo);
   game.state.add('MainMenu', Emigranto.MainMenu);
   game.state.add('Game', Emigranto.Game);
   game.state.add('Loose', Emigranto.Loose);

   //  Now start the Boot state.
   game.state.start('Boot');
};