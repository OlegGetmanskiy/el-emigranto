var prx = ASSETS_LOAD_PREFIX;

var Emigranto = {};

function resizeGame() {
   if(!FULLSCREEN) return;

   var height = $(window).height();
   var width = $(window).width();
   /*if(width < 800) width = 800;
   if(height < 600) height = 600;*/
   if(!mobile) {
      newGameWidth = width * 870 / height;
      if(newGameWidth > 870*2.4)
         newGameWidth = 870*2.4;
      if(newGameWidth < 870*4/3)
         newGameWidth = 870*4/3;

      game.width = newGameWidth;
      game.stage.bounds.width = newGameWidth;
      game.camera.setSize(newGameWidth, game.height);
         
      game.renderer.resize(newGameWidth, 870);
   } else {
      if(height > width) {
         newGameHeight = height / width * 600 ;
         game.width = 600;
         game.height = newGameHeight;
         game.stage.bounds.width = 600;
         game.stage.bounds.height = newGameHeight;
         game.camera.setSize(600, newGameHeight);
         game.renderer.resize(600, newGameHeight);
      } else {
         newGameWidth = width / height * 600;
         game.width = newGameWidth;
         game.height = 600;
         game.stage.bounds.width = newGameWidth;
         game.stage.bounds.height = 600;
         game.camera.setSize(newGameWidth, 600);
         game.renderer.resize(newGameWidth, 600);
      }
   }
   
   Emigranto.doResize();
};

Emigranto.Boot = function (game) {

};

Emigranto.Boot.prototype = {

   preload: function () {
      this.load.image('preloaderBar', prx + 'preloader__2.png');
      this.load.image('preloaderBack1', prx + 'preloader__1.png');
      this.load.image('preloaderBack2', prx + 'preloader__3.png');
   },

   create: function () {

      //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
      this.input.maxPointers = 1;

      //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
      this.stage.disableVisibilityChange = true;
      this.stage.setBackgroundColor('#000000');
      //  Same goes for mobile settings.
      //  In this case we're saying "scale the game, no lower than 480x260 and no higher than 1024x768"
      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      if(mobile || FULLSCREEN) {
         this.scale.minWidth = 0;
         this.scale.minHeight = 0;
         this.scale.maxWidth = this.game.width * 100;
         this.scale.maxHeight = this.game.height * 100;
         this.scale.forceLandscape = false;
         this.scale.pageAlignHorizontally = true;
         this.scale.pageAlignVertically = true;
      } else {
         this.scale.minWidth = 800;
         this.scale.minHeight = 600;
         this.scale.maxWidth = 800;
         this.scale.maxHeight = 600;
      }
      
      this.scale.setScreenSize(true);

      //  By this point the preloader assets have loaded to the cache, we've set the game settings
      //  So now let's start the real preloader going
      resizeGame();
      this.state.start('Preloader');

   }

};