
Emigranto.Logo = function (game) {

};

Emigranto.Logo.prototype = {

   create: function () {
      var logo = this.game.add.sprite(this.game.width/2, this.game.height/2, 'logo');
      this.logo = logo;
      logo.fixedToCamera = true;
      
      logo.anchor.set(0.5);
      logo.alpha = 0;
      this.game.add.tween(logo).to({ alpha: 1 }, 2000, Phaser.Easing.Linear.Out, true);
      this.time.events.add(Phaser.Timer.SECOND * 5, function() {
         this.game.add.tween(logo).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.Out, true);
      }, this);
      this.time.events.add(Phaser.Timer.SECOND * (6.5), function() {
         this.state.start('MainMenu');
      }, this);
   },

   update: function () {
      this.logo.cameraOffset.x = this.game.width/2;
      this.logo.cameraOffset.y = this.game.height/2;
   }

};
