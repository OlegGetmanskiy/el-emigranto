function Joystick(x, y, r, game, group)
{
   this.backgraund = game.add.sprite(x, y, 'bs');
   this.backgraund.anchor.setTo(0.5, 0.5);
   this.sprite = game.add.sprite(x, y, 'js');
   this.sprite.alpha = 0.5;
   this.backgraund.alpha = 0.5;
   this.sprite.anchor.setTo(0.5, 0.5);

   group.add(this.backgraund);
   group.add(this.sprite);

   this.backgraund.fixedToCamera = true;
   this.sprite.fixedToCamera = true;

   //this.sprite.inputEnabled = true;
   this.backgraund.inputEnabled = true;

   this.radius = r;
   this.x = x;
   this.y = y;

   this.pointer = game.input;

   this.isPressed = false;

   this.angle = 0;
}

Joystick.prototype.update = function()
{
this.isPressed = this.backgraund.input.pointerDown(this.pointer.id);

//if(this.isPressed)
var d = distance(this.x, this.y, this.pointer.x, this.pointer.y);
if(d < 75)
{
   this.backgraund.alpha = 0.5;
   this.sprite.alpha = 0.5;
this.angle = angle(this.x, this.y, this.pointer.x, this.pointer.y, true)*57.2957795;

if(d <= this.radius)
{
this.sprite.cameraOffset.x = this.pointer.x;
this.sprite.cameraOffset.y = this.pointer.y;
}
else
{
var p = getMoveVector(this.radius, this.angle)
this.sprite.cameraOffset.x = p.x+this.x;
this.sprite.cameraOffset.y = p.y+this.y;
}
}
else
{
this.sprite.cameraOffset.x = this.x;
this.sprite.cameraOffset.y = this.y;
this.backgraund.alpha = 0.15;
   this.sprite.alpha = 0.15;
//this.angle = 0;
}
}

      function distance(x1, y1, x2, y2)
      {
         var dx = x2 - x1;
         var dy = y2 - y1;
         return Math.sqrt(dx * dx + dy * dy);
      }
 
      function angle(x1, y1, x2, y2, norm)
      {
         var dx = x2 - x1;
         var dy = y2 - y1;
         var angle = Math.atan2(dy, dx);
         
         if (norm)
         {
            if (angle < 0)
            {
               angle = Math.PI * 2 + angle;  
            }
            else if (angle >= Math.PI * 2)
            {
               angle = angle - Math.PI * 2;
            }
         }
         
         return angle;
      }

      function getMoveVector(speed, angle)
      {
         var vec = {'x':0, 'y':0};
         angle = angle / 57.2957795;
         vec.x = speed * Math.cos(angle);
         vec.y = speed * Math.sin(angle);
         
         return vec;       
      }